﻿using UnityEngine;
using System.Collections;

public class EnemySpawning : MonoBehaviour {
	public bool DONT_SPAWN;
	public ObjectPooler pool;
	public GameObject enemyOG;
    public GameObject enemyNotOG;
    public float timeBetweenEnemies2 = 500f;
	public float timeBetweenEnemies = 1.5f;
    private bool isreadyOG = true;
    private bool isreadyNotOG = true;

	void Start(){
		pool.ADV_ENEMY_POOL = new GameObject[pool.ADV_ENEMIES_ALLOTED];
		for(int i = 0; i < pool.ADV_ENEMY_POOL.Length; i++){
			pool.ADV_ENEMY_POOL[i] = (GameObject) Instantiate(enemyNotOG,pool.transform.position,pool.transform.rotation);
			pool.ADV_ENEMY_POOL[i].SetActive(false);
		}
	}

	// Update is called once per frame

	void Update () {

        if (isreadyOG)
        {
			isreadyOG = false;
            StartCoroutine(SpawnEnemyOG());
           
        }
        if (isreadyNotOG)
        {
			isreadyNotOG = false;
            StartCoroutine(SpawnEnemyNotOG());
            
        }
    
	}
    IEnumerator SpawnEnemyOG()
    {
		int ENEMIES_ACTIVE = 0;
		if(DONT_SPAWN != true){
		
		for(int i = 0; i < pool.ENEMY_POOL.Length; i++){
			if(pool.ENEMY_POOL[i].activeInHierarchy == false){
				pool.ENEMY_POOL[i].transform.position = new Vector3(transform.position.x,Random.Range(-3f,4.5f),transform.position.z);
				pool.ENEMY_POOL[i].SetActive(true);
				break;
			}else{
				
				ENEMIES_ACTIVE++;
			}
			
		}
		}
		if(ENEMIES_ACTIVE == pool.ENEMIES_ALLOTED){
			GameObject[] TEMP_POOL = new GameObject[pool.ENEMIES_ALLOTED + 1];
			pool.ENEMY_POOL.CopyTo(TEMP_POOL,0);
			pool.ENEMY_POOL = TEMP_POOL;
			pool.ENEMIES_ALLOTED = pool.ENEMIES_ALLOTED + 1;
			pool.ENEMY_POOL[pool.ENEMY_POOL.Length - 1] = (GameObject) Instantiate(pool.ENEMY,transform.position,transform.rotation);
			pool.ENEMY_POOL[pool.ENEMY_POOL.Length - 1].SetActive(true);
		}
		yield return new WaitForSeconds(timeBetweenEnemies);
		isreadyOG = true;
    }
    IEnumerator SpawnEnemyNotOG()
    {
		int ADV_ENEMIES_ACTIVE = 0;
		if(DONT_SPAWN != true){
		for(int i = 0; i < pool.ADV_ENEMY_POOL.Length; i++){
			if(pool.ADV_ENEMY_POOL[i].activeInHierarchy == false){
				pool.ADV_ENEMY_POOL[i].transform.position = new Vector3(transform.position.x,Random.Range(-3f,4.5f),transform.position.z);
				pool.ADV_ENEMY_POOL[i].SetActive(true);
				break;
			}else{
				
				ADV_ENEMIES_ACTIVE++;
			}
			
		}
		}
		if(ADV_ENEMIES_ACTIVE == pool.ADV_ENEMIES_ALLOTED){
			GameObject[] TEMP_POOL = new GameObject[pool.ADV_ENEMIES_ALLOTED + 1];
			pool.ADV_ENEMY_POOL.CopyTo(TEMP_POOL,0);
			pool.ADV_ENEMY_POOL = TEMP_POOL;
			pool.ADV_ENEMIES_ALLOTED = pool.ADV_ENEMIES_ALLOTED + 1;
			pool.ADV_ENEMY_POOL[pool.ADV_ENEMY_POOL.Length - 1] = (GameObject) Instantiate(enemyNotOG,transform.position,transform.rotation);
			pool.ADV_ENEMY_POOL[pool.ADV_ENEMY_POOL.Length - 1].SetActive(true);
		}
        yield return new WaitForSeconds(timeBetweenEnemies2);
        isreadyNotOG = true;


    }
}
