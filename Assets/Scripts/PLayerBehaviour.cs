﻿using UnityEngine;
using System.Collections;

public class PLayerBehaviour : MonoBehaviour {
	public Sprite OtherPlane;
	public Sprite Original;
	public Sprite[] Palettes;
	public int playerSpeed = 10;
	public AudioClip Jump;
	public BalloonHitDetection ender;
	public SpriteRenderer phony;
	public GameObject BG;
	public string Name;
	// Use this for initialization
	void Start () {
		int randoPalette = Random.Range (0, 8);
		phony.sprite = Palettes [randoPalette];
		Original = Palettes [randoPalette];
		OtherPlane = Palettes [randoPalette + 8];

	}
	
	// Update is called once per frame
	void Update () {
		Vector3 v3 = Input.mousePosition;
		Vector2 player = transform.position;
		Rigidbody2D mi = GetComponent<Rigidbody2D>();
		if (Input.GetKeyDown (KeyCode.LeftShift)) {
			PlayerPrefs.DeleteAll();

		}
		if (ender.isDead == false) {
			if (Input.GetMouseButton (0) || Input.GetKey(KeyCode.Space)) {

				mi.gravityScale = 0f;
				mi.velocity.Set (0f, 0f);
				mi.isKinematic = true;
				mi.isKinematic = false;
				v3 = Camera.main.ScreenToWorldPoint (v3);
				if (player.y < v3.y - .5f) {

					phony.sprite = OtherPlane;
				}
				if (player.y < v3.y) {
					transform.Translate (0f, playerSpeed * Time.deltaTime, 0f);

				} else {

					phony.sprite = Original;
				}

			}
			if (Input.GetMouseButtonDown (0)) {
				AudioSource.PlayClipAtPoint (Jump, transform.position, .25f);
			}
			if (Input.GetMouseButtonUp (0) || Input.GetKeyUp(KeyCode.Space)) {

				phony.sprite = Original;
				mi.gravityScale = 1f;
			}
		}
	}
}
