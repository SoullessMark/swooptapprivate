﻿using UnityEngine;
using System.Collections;

public class BalloonHitDetection : MonoBehaviour {
	public ObjectPooler pool;
	public GameObject Poof;
	public float deathTimer = 5f;
	public float loadTime = 250f;
	public bool canLoad = false;
	public bool isDead = false;
	public AudioClip hit;
	public int score = 0;
	public int hiScore = 0;
	public Animator b;
	// Use this for initialization
	void Start () {
		score = 0;
		hiScore = PlayerPrefs.GetInt ("Score");
		b = GameObject.FindWithTag ("Over").GetComponent<Animator> ();
		pool = GameObject.FindWithTag("Pooler").GetComponent<ObjectPooler>();
	}
	
	// Update is called once per frame
	void Update () {
		canLoad = GameObject.FindWithTag ("Loader").GetComponent<MayLoad> ().canLoad;
		float second = Time.deltaTime * 60;
		if (isDead) {
			deathTimer -= second;
			if(deathTimer <= 0){
				Animator a = GetComponentInParent<Animator>();
				a.SetInteger("State",0);
				b.SetInteger("State",1);
				if(canLoad){
					loadTime -= second;
					if(loadTime <= 0){
						Application.LoadLevel("DaBaseGame");
					}
				}
			}
		}
		if (score > hiScore) {
			PlayerPrefs.SetInt("Score",score);
			PlayerPrefs.Save();
		}
	}
	void OnTriggerEnter2D(Collider2D col){
		if (isDead == false) {
			if (col.gameObject.tag == "Balloon") {
				AudioClip Pop = col.gameObject.GetComponent<BalloonBehaviour> ().Pop;
				AudioSource.PlayClipAtPoint (Pop, transform.position, .25f);
				for (int i = 0; i < 5; i++) {
					int POOFS_ACTIVE = 0;
					for(int k = 0; k < pool.POOF_POOL.Length; k++){
						if(pool.POOF_POOL[i].activeInHierarchy == false){
							pool.POOF_POOL[i].transform.position = new Vector3(col.transform.position.x,col.transform.position.y,transform.position.z);
							pool.POOF_POOL[i].SetActive(true);
							break;
						}else{
							
							POOFS_ACTIVE++;
						}
						
					}
					if(POOFS_ACTIVE == pool.POOFS_ALLOTED){
						GameObject[] TEMP_POOL = new GameObject[pool.POOFS_ALLOTED + 1];
						pool.POOF_POOL.CopyTo(TEMP_POOL,0);
						pool.POOF_POOL = TEMP_POOL;
						pool.POOFS_ALLOTED = pool.POOFS_ALLOTED + 1;
						pool.POOF_POOL[pool.POOF_POOL.Length - 1] = (GameObject) Instantiate(pool.POOF,col.transform.position,col.transform.rotation);
						pool.POOF_POOL[pool.POOF_POOL.Length - 1].SetActive(true);
					}
				}
				score++;
				col.gameObject.SetActive(false);
				col.gameObject.transform.position = GameObject.FindWithTag("Pooler").transform.position;
			}
			if (col.gameObject.tag == "Death" || col.gameObject.tag == "Bounce") {

				AudioSource.PlayClipAtPoint (hit, transform.position, .25f);
				Animator a = GetComponentInParent<Animator> ();
				a.SetInteger ("State", 1);
				PlayerPrefs.Save();
				isDead = true;
			}
		}
	}

}
