﻿using UnityEngine;
using System.Collections;

public class Initializer : MonoBehaviour {
	public int isInitialized = 0;
	public Selectable[] selectables;

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt (selectables [0].storedName, 1);

		isInitialized = PlayerPrefs.GetInt ("Initialized");
		if (isInitialized == 0) {
			for(int i = 1; i < selectables.Length; i++){
				PlayerPrefs.SetInt(selectables[i].storedName,0);

			}
			isInitialized = 1;
			PlayerPrefs.SetInt ("Initialized", isInitialized);
			PlayerPrefs.Save();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
