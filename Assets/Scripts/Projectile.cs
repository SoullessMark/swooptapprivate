﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    public float ProjectileSpeed;
	private float STORED_SPEED;
	public int TIME_BEFORE_DEATH = 5;
	private bool TIMER_STARTED = false;
	// Use this for initialization
	void Start () {

		STORED_SPEED = ProjectileSpeed;
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Vector2.left * (ProjectileSpeed * Time.deltaTime));
        if(!TIMER_STARTED){
			TIMER_STARTED = true;
			StartCoroutine(DEATH_TIMER());

		}
		if(ProjectileSpeed == 0){
			ProjectileSpeed = STORED_SPEED;
		}

    }
	IEnumerator DEATH_TIMER(){

		yield return new WaitForSeconds(TIME_BEFORE_DEATH);
		gameObject.SetActive(false);
		transform.position = GameObject.FindWithTag("Pooler").transform.position;
		TIMER_STARTED = false;
		ProjectileSpeed = STORED_SPEED;
	}
}
