﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {
	public float enemySpeed = 1f;
	public float enemyScale = 1f;
	public Sprite[] Enemies;
	public SpriteRenderer me;
	public AudioClip hit;
	// Use this for initialization
	void Start () {
		enemySpeed = Random.Range (.5f, 1.2f);
		enemyScale = Random.Range (.4f, 1.2f);
		me = GetComponent<SpriteRenderer> ();
		int randoEnemy = Random.Range (0, 6);
		me.sprite = Enemies [randoEnemy];
		transform.localScale = new Vector3 (enemyScale, enemyScale, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (0f, -1f * (enemySpeed / 3) * Time.deltaTime, 0f);
		transform.Translate (-1f * (enemySpeed * 2) * Time.deltaTime, 0f, 0f);
		Transform Player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ();
		if (transform.position.x < (Player.position.x - 3)) {
			gameObject.SetActive(false);
			transform.position = GameObject.FindWithTag("Pooler").transform.position;
		}
	}
}
