﻿using UnityEngine;
using System.Collections;

public class BalloonSpawning : MonoBehaviour {
	public GameObject Balloon;
	public float timeBetweenBalloons = 10f;
	public ObjectPooler pool;
	private bool IS_READY = true;
	private bool SPAWNED_BOSS;
	public int BALLOONS_BEFORE_BOSS = 25;
	public int TIER_COUNT = 1;
	public bool DONT_SPAWN = false;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	if(IS_READY){

			IS_READY = false;
			StartCoroutine(SPAWN_BALLOON());
		}
	if(!SPAWNED_BOSS && GameObject.FindWithTag("Finish").GetComponent<BalloonHitDetection>().score == BALLOONS_BEFORE_BOSS){
			SPAWNED_BOSS = true;
			StartCoroutine(SPAWN_BOSS());
	}

	}
	IEnumerator SPAWN_BALLOON(){
		int BALLOONS_ACTIVE = 0;
		if(!DONT_SPAWN){
		for(int i = 0; i < pool.BALLOON_POOL.Length; i++){
			if(pool.BALLOON_POOL[i].activeInHierarchy == false){
				pool.BALLOON_POOL[i].transform.position = new Vector3(transform.position.x,Random.Range(-5,2.5f),transform.position.z);
				pool.BALLOON_POOL[i].SetActive(true);
				break;
			}else{

				BALLOONS_ACTIVE++;
			}

		}
		}
		if(BALLOONS_ACTIVE == pool.BALLOONS_ALLOTED){
			GameObject[] TEMP_POOL = new GameObject[pool.BALLOONS_ALLOTED + 1];
			pool.BALLOON_POOL.CopyTo(TEMP_POOL,0);
			pool.BALLOON_POOL = TEMP_POOL;
			pool.BALLOONS_ALLOTED = pool.BALLOONS_ALLOTED + 1;
			pool.BALLOON_POOL[pool.BALLOON_POOL.Length - 1] = (GameObject) Instantiate(pool.BALLOON,transform.position,transform.rotation);
			pool.BALLOON_POOL[pool.BALLOON_POOL.Length - 1].SetActive(true);
		}
		yield return new WaitForSeconds(timeBetweenBalloons);
		IS_READY = true;
	}
	IEnumerator SPAWN_BOSS(){
		if(TIER_COUNT == 1){

			if(pool.BOSSES[1].activeInHierarchy == false){
			pool.BOSSES[1].gameObject.SetActive(true);
			pool.BOSSES[1].transform.position = new Vector3(0,0,0);
			pool.BOSSES[1].gameObject.GetComponent<BossBehaviour>().ON_START();
				Debug.Log ("This Blows");
			}
		}else if(TIER_COUNT == 2){

		}else{
			TIER_COUNT = 0;
		}
		TIER_COUNT++;
		BALLOONS_BEFORE_BOSS += 5;
		SPAWNED_BOSS = false;
		yield return null;
	}
}
