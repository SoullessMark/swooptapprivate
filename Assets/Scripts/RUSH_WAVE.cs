﻿using UnityEngine;
using System.Collections;

public class RUSH_WAVE : MonoBehaviour {
	public GameObject pooler;
	public int MAX_PROJ;
	public GameObject[] PROJ_POOL;
	public GameObject projectile;
	public int ASSIGNED_ABILITY_NUM;
	public BossBehaviour boss;
	private Animator SPAWNER;
	private bool HAS_SHOT = false;
	// Use this for initialization
	void Start () {
		SPAWNER = GetComponentInChildren<Animator>();
		boss = gameObject.GetComponent<BossBehaviour>();
		pooler = GameObject.FindWithTag("Pooler");
		PROJ_POOL = new GameObject[MAX_PROJ];
		for(int i = 0; i < PROJ_POOL.Length; i++){
			PROJ_POOL[i] = (GameObject) Instantiate(projectile,pooler.transform.position,pooler.transform.rotation);
			PROJ_POOL[i].SetActive(false);
		}
	}
	
	// Update is called once per frame
	/*void Update () {
	if(ASSIGNED_ABILITY_NUM == (boss.ACTIVE_ABILITY - 1) && !HAS_SHOT && boss.USE_NOW){
			HAS_SHOT = true;
			StartCoroutine(ABILITY_GO());
		}
	}
	IEnumerator ABILITY_GO(){
		int PROJ_ACTIVE = 0;
		yield return new WaitForSeconds(((boss.ABILITIES[boss.ACTIVE_ABILITY - 1].length - .375f) / 3) / boss.GetComponentInChildren<Animator>().speed);
		for(int i = 0; i < PROJ_POOL.Length; i++){
			if(PROJ_POOL[i].activeInHierarchy == false){
				PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}

		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			PROJ_POOL.CopyTo(TEMP_POOL,0);
			PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			PROJ_POOL[PROJ_POOL.Length - 1] = (GameObject) Instantiate(projectile,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);
			PROJ_POOL[PROJ_POOL.Length - 1].SetActive(true);
		}
		StartCoroutine(ABILITY_GOING());
	}
	IEnumerator ABILITY_GOING(){
		int PROJ_ACTIVE = 0;
		yield return new WaitForSeconds(((boss.ABILITIES[boss.ACTIVE_ABILITY - 1].length - .375f) / 3) / boss.GetComponentInChildren<Animator>().speed);
		for(int i = 0; i < PROJ_POOL.Length; i++){
			if(PROJ_POOL[i].activeInHierarchy == false){
				PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			PROJ_POOL.CopyTo(TEMP_POOL,0);
			PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			PROJ_POOL[PROJ_POOL.Length - 1] = (GameObject) Instantiate(projectile,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);;
			PROJ_POOL[PROJ_POOL.Length - 1].SetActive(true);
		}
		StartCoroutine(ABILITY_ENDER());
	}
	IEnumerator ABILITY_ENDER(){
		int PROJ_ACTIVE = 0;
		yield return new WaitForSeconds(((boss.ABILITIES[boss.ACTIVE_ABILITY - 1].length - .375f) / 3) / boss.GetComponentInChildren<Animator>().speed);
		for(int i = 0; i < PROJ_POOL.Length; i++){
			if(PROJ_POOL[i].activeInHierarchy == false){
				PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			PROJ_POOL.CopyTo(TEMP_POOL,0);
			PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			PROJ_POOL[PROJ_POOL.Length - 1] = (GameObject) Instantiate(projectile,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);
			PROJ_POOL[PROJ_POOL.Length - 1].SetActive(true);
		}
		HAS_SHOT = false;
		boss.USE_NOW = false;
	}
*/
	public void BOOP(){
		int PROJ_ACTIVE = 0;

		for(int i = 0; i < PROJ_POOL.Length; i++){
			if(PROJ_POOL[i].activeInHierarchy == false){
				PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				PROJ_POOL[i].transform.rotation = SPAWNER.transform.rotation;
				PROJ_POOL[i].SetActive(true);
				PROJ_POOL[i].GetComponent<SpriteRenderer>().sprite = boss.PROJ_SPRITE;
				PROJ_POOL[i].GetComponent<Projectile>().ProjectileSpeed = PROJ_POOL[i].GetComponent<Projectile>().ProjectileSpeed * (boss.GetComponentInChildren<Animator>().speed - .5f);
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			PROJ_POOL.CopyTo(TEMP_POOL,0);
			PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			PROJ_POOL[PROJ_POOL.Length - 1] = (GameObject) Instantiate(projectile,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);
			PROJ_POOL[PROJ_POOL.Length - 1].SetActive(true);
		}
	}
}
