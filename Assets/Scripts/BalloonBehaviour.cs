﻿using UnityEngine;
using System.Collections;

public class BalloonBehaviour : MonoBehaviour {
	public float balloonSpeed = 1f;
	public Sprite[] Balloons;
	public SpriteRenderer me;
	public AudioClip Pop;
	// Use this for initialization
	void Start () {
		balloonSpeed = Random.Range (.5f, 1f);
		me = GetComponent<SpriteRenderer> ();
		int randoBalloonColor = Random.Range (0, 5);
		me.sprite = Balloons [randoBalloonColor];
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (0f, (balloonSpeed / 3) * Time.deltaTime, 0f);
		transform.Translate (-1 * (balloonSpeed * 2) * Time.deltaTime, 0f, 0f);
		Transform Player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ();
		if (transform.position.x < (Player.position.x - 10)) {
			gameObject.SetActive(false);
			transform.position = GameObject.FindWithTag("Pooler").transform.position;
		}
	}
}
