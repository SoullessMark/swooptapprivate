﻿using UnityEngine;
using System.Collections;

public class CoinBehaviour : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(-.02f, 0f, 0f);
        Transform Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        if (transform.position.x < (Player.position.x - 10))
        {
            gameObject.SetActive(false);
            transform.position = GameObject.FindWithTag("Pooler").transform.position;
        }
    }
}
