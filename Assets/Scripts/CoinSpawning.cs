﻿using UnityEngine;
using System.Collections;

public class CoinSpawning : MonoBehaviour {
    public GameObject Coin;
    public float timeBetweenCoins = 30f;
    public ObjectPooler pool;
    private bool IS_READY = true;

    // Use this for initialization
    void Start () {
        pool.COIN_POOL = new GameObject[pool.COINS_ALLOTED];
        for (int i = 0; i < pool.COIN_POOL.Length; i++)
        {
            pool.COIN_POOL[i] = (GameObject)Instantiate(Coin, pool.transform.position, pool.transform.rotation);
            pool.COIN_POOL[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IS_READY)
        {

            IS_READY = false;
            StartCoroutine(SPAWN_COIN());

        }
    }

    IEnumerator SPAWN_COIN()
    {
        int COINS_ACTIVE = 0;
        for (int i = 0; i < pool.COIN_POOL.Length; i++)
        {
            if (pool.COIN_POOL[i].activeInHierarchy == false)
            {
                pool.COIN_POOL[i].transform.position = new Vector3(transform.position.x, Random.Range(-5, 2.5f), transform.position.z);
                pool.COIN_POOL[i].SetActive(true);
                break;
            }
            else {

                COINS_ACTIVE++;
            }

        }
        if (COINS_ACTIVE == pool.COINS_ALLOTED)
        {
            GameObject[] TEMP_POOL = new GameObject[pool.COINS_ALLOTED + 1];
            pool.COIN_POOL.CopyTo(TEMP_POOL, 0);
            pool.COIN_POOL = TEMP_POOL;
            pool.COINS_ALLOTED = pool.COINS_ALLOTED + 1;
            pool.COIN_POOL[pool.COIN_POOL.Length - 1] = (GameObject)Instantiate(Coin, transform.position, transform.rotation);
            pool.COIN_POOL[pool.COIN_POOL.Length - 1].SetActive(true);
        }
        yield return new WaitForSeconds(timeBetweenCoins);
        IS_READY = true;
    }
}
