﻿using UnityEngine;
using System.Collections;

public class BOUNCE_BARRAGE : MonoBehaviour {
	public int ASSIGNED_ABILITY_NUM;
	public int MAX_PROJ;
	public GameObject BOUNCY_PROJ;
	public GameObject[] BOUNCY_PROJ_POOL;
	private ObjectPooler pool;
	private Animator SPAWNER;
	private BossBehaviour boss;
	private bool HAS_SHOT = false;
	// Use this for initialization
	void Start () {
		boss = gameObject.GetComponent<BossBehaviour>();
		SPAWNER = gameObject.GetComponentInChildren<Animator>();
		pool = GameObject.FindWithTag("Pooler").GetComponent<ObjectPooler>();
		BOUNCY_PROJ_POOL = new GameObject[MAX_PROJ];
		for(int i = 0; i < BOUNCY_PROJ_POOL.Length; i++){
			BOUNCY_PROJ_POOL[i] = (GameObject) Instantiate(BOUNCY_PROJ, pool.transform.position,pool.transform.rotation);
			BOUNCY_PROJ_POOL[i].SetActive(false);
		}
	}
	
	// Update is called once per frame
/*	void Update () {
		if(ASSIGNED_ABILITY_NUM == (boss.ACTIVE_ABILITY - 1) && !HAS_SHOT && boss.USE_NOW){
			HAS_SHOT = true;
			StartCoroutine(ABILITY_GO());
		}
	}
	IEnumerator ABILITY_GO(){
		int PROJ_ACTIVE = 0;
		yield return new WaitForSeconds(((boss.ABILITIES[boss.ACTIVE_ABILITY - 1].length + .1f) / 3) / boss.GetComponentInChildren<Animator>().speed);
		for(int i = 0; i < BOUNCY_PROJ_POOL.Length; i++){
			if(BOUNCY_PROJ_POOL[i].activeInHierarchy == false){
				BOUNCY_PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				BOUNCY_PROJ_POOL[i].transform.rotation = SPAWNER.transform.rotation;
				BOUNCY_PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			BOUNCY_PROJ_POOL.CopyTo(TEMP_POOL,0);
			BOUNCY_PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1] = (GameObject) Instantiate(BOUNCY_PROJ,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].SetActive(true);
		}
		StartCoroutine(ABILITY_GOING());
	}
	IEnumerator ABILITY_GOING(){
		int PROJ_ACTIVE = 0;
		yield return new WaitForSeconds(((boss.ABILITIES[boss.ACTIVE_ABILITY - 1].length + .1f) / 3) / boss.GetComponentInChildren<Animator>().speed);
		for(int i = 0; i < BOUNCY_PROJ_POOL.Length; i++){
			if(BOUNCY_PROJ_POOL[i].activeInHierarchy == false){
				BOUNCY_PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				BOUNCY_PROJ_POOL[i].transform.rotation = SPAWNER.transform.rotation;
				BOUNCY_PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			BOUNCY_PROJ_POOL.CopyTo(TEMP_POOL,0);
			BOUNCY_PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1] = (GameObject) Instantiate(BOUNCY_PROJ,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);;
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].SetActive(true);
		}
		StartCoroutine(ABILITY_ENDER());
	}
	IEnumerator ABILITY_ENDER(){
		int PROJ_ACTIVE = 0;
		yield return new WaitForSeconds(((boss.ABILITIES[boss.ACTIVE_ABILITY - 1].length - .15f) / 3) / boss.GetComponentInChildren<Animator>().speed);
		for(int i = 0; i < BOUNCY_PROJ_POOL.Length; i++){
			if(BOUNCY_PROJ_POOL[i].activeInHierarchy == false){
				BOUNCY_PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				BOUNCY_PROJ_POOL[i].transform.rotation = SPAWNER.transform.rotation;
				BOUNCY_PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			BOUNCY_PROJ_POOL.CopyTo(TEMP_POOL,0);
			BOUNCY_PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1] = (GameObject) Instantiate(BOUNCY_PROJ,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].SetActive(true);
		}
		HAS_SHOT = false;
		boss.USE_NOW = false;
	}*/
	public void SHOOT_PROJ(){
		int PROJ_ACTIVE = 0;

		for(int i = 0; i < BOUNCY_PROJ_POOL.Length; i++){
			if(BOUNCY_PROJ_POOL[i].activeInHierarchy == false){
				BOUNCY_PROJ_POOL[i].transform.position = new Vector3(SPAWNER.gameObject.transform.position.x,SPAWNER.gameObject.transform.position.y,transform.position.z);
				BOUNCY_PROJ_POOL[i].transform.rotation = SPAWNER.transform.rotation;
				BOUNCY_PROJ_POOL[i].GetComponent<Projectile>().ProjectileSpeed = BOUNCY_PROJ_POOL[i].GetComponent<Projectile>().ProjectileSpeed * (boss.GetComponentInChildren<Animator>().speed - .5f);
				BOUNCY_PROJ_POOL[i].GetComponent<SpriteRenderer>().sprite = boss.PROJ_SPRITE;
				BOUNCY_PROJ_POOL[i].SetActive(true);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		if(PROJ_ACTIVE == MAX_PROJ){
			GameObject[] TEMP_POOL = new GameObject[MAX_PROJ + 1];
			BOUNCY_PROJ_POOL.CopyTo(TEMP_POOL,0);
			BOUNCY_PROJ_POOL = TEMP_POOL;
			MAX_PROJ = MAX_PROJ + 1;
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1] = (GameObject) Instantiate(BOUNCY_PROJ,SPAWNER.gameObject.transform.position,SPAWNER.gameObject.transform.rotation);
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].GetComponent<Projectile>().ProjectileSpeed = BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].GetComponent<Projectile>().ProjectileSpeed * (boss.GetComponentInChildren<Animator>().speed - .5f);
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].GetComponent<SpriteRenderer>().sprite = boss.PROJ_SPRITE;
			BOUNCY_PROJ_POOL[BOUNCY_PROJ_POOL.Length - 1].SetActive(true);
		}
	}
}
