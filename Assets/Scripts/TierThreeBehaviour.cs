﻿using UnityEngine;
using System.Collections;

public class TierThreeBehaviour : MonoBehaviour {
    public GameObject Projectile;
	public GameObject[] PROJ_POOL;
	public int PROJ_ALOTTED;
	public float ANIM_SHOOT_SPEED;
    public float EnemySpeed;
    public float EnemyProjectileTimer;
    public bool hasShot = false;
    public AnimationClip charge;
	private GameObject pool;
	private int MAX_SHOTS;

	// Use this for initialization
	void Start ()
    {
		pool = GameObject.FindWithTag("Pooler");
		PROJ_POOL = new GameObject[PROJ_ALOTTED];
		for (int i = 0; i < PROJ_POOL.Length; i++){
			PROJ_POOL[i] = (GameObject) Instantiate(Projectile, pool.transform.position,pool.transform.rotation);
			PROJ_POOL[i].SetActive(false);
		}
        EnemySpeed = Random.Range(1.5f, 4f);
        EnemyProjectileTimer = Random.Range(1.5f, 2.5f);
		MAX_SHOTS = Random.Range (0,4);

	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector2.left * (EnemySpeed * Time.deltaTime));
        if (transform.position.x <= GameObject.FindGameObjectWithTag("Player").transform.position.x - 10)
        {
			gameObject.SetActive(false);
			transform.position = GameObject.FindWithTag("Pooler").transform.position;
			hasShot = false;
			MAX_SHOTS = Random.Range (0,4);
		}
		if (!hasShot && gameObject.activeInHierarchy && MAX_SHOTS < 3)
        {
            hasShot = true;
            StartCoroutine(Shoot());

        }
        
        
	}

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(EnemyProjectileTimer);
        GetComponent<Animator>().SetInteger("State", 1);
		GetComponent<Animator>().speed = ANIM_SHOOT_SPEED;
        EnemySpeed = 0;
        StartCoroutine(ProjectileSpawn());



    }

    IEnumerator ProjectileSpawn()
    {
		int PROJ_ACTIVE = 0;
        yield return new WaitForSeconds((charge.length / ANIM_SHOOT_SPEED) - .01f);

		for(int i = 0; i < PROJ_POOL.Length; i++){
			if(PROJ_POOL[i].activeInHierarchy == false){
				PROJ_POOL[i].transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z);
				PROJ_POOL[i].SetActive(true);
				PROJ_POOL[i].transform.eulerAngles = new Vector3(0,0,25);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		for(int i = 0; i < PROJ_POOL.Length; i++){
			if(PROJ_POOL[i].activeInHierarchy == false){
				PROJ_POOL[i].transform.position = new Vector3(transform.position.x,transform.position.y,transform.position.z);
				PROJ_POOL[i].SetActive(true);
				PROJ_POOL[i].transform.eulerAngles = new Vector3(0,0,-25);
				break;
			}else{
				PROJ_ACTIVE ++;
			}
			
		}
		
		if(PROJ_ACTIVE == PROJ_ALOTTED){
			GameObject[] TEMP_POOL = new GameObject[PROJ_ALOTTED + 1];
			PROJ_POOL.CopyTo(TEMP_POOL,0);
			PROJ_POOL = TEMP_POOL;
			PROJ_ALOTTED = PROJ_ALOTTED + 1;
			PROJ_POOL[PROJ_POOL.Length - 1] = (GameObject) Instantiate(Projectile,transform.position,transform.rotation);
			PROJ_POOL[PROJ_POOL.Length - 1].SetActive(true);
		}
       
		StartCoroutine(ANIM_RESET());

    }
	IEnumerator ANIM_RESET(){

		yield return new WaitForSeconds(charge.length / ANIM_SHOOT_SPEED - .5f);
		GetComponent<Animator>().SetInteger("State", 0);
		GetComponent<Animator>().speed = 1f;
		EnemySpeed = Random.Range(1.5f, 4f);
		hasShot = false;
		MAX_SHOTS++;
	}
}
