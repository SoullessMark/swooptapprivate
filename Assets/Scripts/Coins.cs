﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Coins : MonoBehaviour
{
    public BalloonHitDetection detect;
    // Use this for initialization
    void Start()
    {
        detect = GameObject.FindWithTag("Finish").GetComponent<BalloonHitDetection>();
    }

    // Update is called once per frame
    void Update()
    {
        int i = PlayerPrefs.GetInt("Coins");

        Text me = GetComponent<Text>();
        me.text = i + "¢";
    }
}