﻿using UnityEngine;
using System.Collections;

public class BossBehaviour : MonoBehaviour {
	public Sprite PROJ_SPRITE;
	public int OVERRIDE_TIME;
	private ObjectPooler pool;
	public AnimationClip[] ABILITIES;
	public int ACTIVE_ABILITY;
	public int MAX_ABILITIES;
	public int BUFFER_TIMER;
	private int STORED_BUFFER;
	private Animator BOSS;
	public int TIME_ALOTTED;
	private int STORED_TIME_ALOTTED;
	public int BOSS_TIER;
	private bool SUBTRACTED_SECOND = false;
	public bool CAN_USE_ABILITY = false;
	public bool IS_FINISHED = false;
	public bool IS_BUFFERED = false;
	private bool HAS_DIED = false;
	public bool USE_NOW = false;
	private bool DONT_SPAWN = false;
	private EnemySpawning SPAWNER;
	// Use this for initialization
	void Start () {
		SPAWNER = GameObject.FindWithTag("Spawner").GetComponent<EnemySpawning>();
		STORED_BUFFER = BUFFER_TIMER;
		pool = GameObject.FindWithTag("Pooler").GetComponent<ObjectPooler>();
		ACTIVE_ABILITY = Random.Range (2,MAX_ABILITIES + 2);
		BOSS = gameObject.GetComponentInChildren<Animator>();
		BOSS.speed = 1.5f;
		ON_START();
		if(OVERRIDE_TIME <= 1){
		if(BOSS_TIER == 1){
		TIME_ALOTTED = Random.Range(50,60);
		}else if(BOSS_TIER == 2){
		TIME_ALOTTED = Random.Range(60,70);
		}else{
		TIME_ALOTTED = Random.Range(70,80);
		}
		}else if(OVERRIDE_TIME > 0){
			TIME_ALOTTED = OVERRIDE_TIME;
		}
		STORED_TIME_ALOTTED = TIME_ALOTTED;
	}
	
	// Update is called once per frame
	void Update () {



		if(!SUBTRACTED_SECOND && !HAS_DIED){
			SUBTRACTED_SECOND = true;
			StartCoroutine(ACTIVE_TIMER());

		}
		if(!CAN_USE_ABILITY && IS_FINISHED){
			CAN_USE_ABILITY = true;
			IS_FINISHED = false;
			StartCoroutine(BUFFER());
			Debug.Log("Hey");
		}

		if(CAN_USE_ABILITY && IS_BUFFERED && TIME_ALOTTED > 0){
			CAN_USE_ABILITY = false;
			IS_BUFFERED = false;
			USE_NOW = true;
			StartCoroutine(PLAY_ABILITY_ANIM());
			Debug.Log("Hey");
		}
		if(TIME_ALOTTED <= (STORED_TIME_ALOTTED / 2)){
			BOSS.speed = 2f;
			BUFFER_TIMER = STORED_BUFFER + 1;
		}
		if(TIME_ALOTTED <= 0 && !HAS_DIED){
			HAS_DIED = true;
			TIME_ALOTTED = STORED_TIME_ALOTTED;
			BOSS.SetInteger("State", 0);
			StartCoroutine(ON_DEATH());
		}
	}
	public void ON_START(){
		this.gameObject.SetActive(true);
		GameObject.FindWithTag("Spawner").GetComponent<EnemySpawning>().DONT_SPAWN = true;
		BOSS = gameObject.GetComponentInChildren<Animator>();
		BOSS.speed = 1f;
		StartCoroutine(START_FIGHT());

	}
	IEnumerator START_FIGHT(){
		CAN_USE_ABILITY = false;
		IS_FINISHED = false;
		IS_BUFFERED = false;
		USE_NOW = false;
		BOSS.SetInteger("State",MAX_ABILITIES + 2);
		yield return new WaitForSeconds(ABILITIES[MAX_ABILITIES + 1].length / BOSS.speed);
		BOSS.speed = 1.5f;
		BOSS.SetInteger("State",0);
		IS_FINISHED = true;

	}
	IEnumerator ACTIVE_TIMER(){

		yield return new WaitForSeconds(1);
		SUBTRACTED_SECOND = false;
		TIME_ALOTTED--;

	}
	IEnumerator BUFFER(){
		BOSS.SetInteger("State",0);
		CAN_USE_ABILITY = false;
		IS_BUFFERED = false;
		yield return new WaitForSeconds(BUFFER_TIMER);
		IS_BUFFERED = true;
		CAN_USE_ABILITY = true;
		ACTIVE_ABILITY = Random.Range (2,MAX_ABILITIES + 2);
	}
	IEnumerator PLAY_ABILITY_ANIM(){
		CAN_USE_ABILITY = false;
		IS_FINISHED = false;
		BOSS.SetInteger("State",ACTIVE_ABILITY - 1);
		yield return new WaitForSeconds(ABILITIES[ACTIVE_ABILITY -1].length / BOSS.speed);
		BOSS.SetInteger("State",0);
		IS_FINISHED = true;
		USE_NOW = false;
	}
	IEnumerator ON_DEATH(){
		BOSS.speed = .5f;
		BOSS.SetInteger("State",MAX_ABILITIES + 1);
		yield return new WaitForSeconds(ABILITIES[MAX_ABILITIES + 1].length / BOSS.speed);
		transform.position = pool.transform.position;
		HAS_DIED = false;
		BOSS.gameObject.transform.eulerAngles = new Vector3(0,0,0);
		gameObject.SetActive(false);
		GameObject.FindWithTag("Spawner").GetComponent<EnemySpawning>().DONT_SPAWN = false;
		GameObject.FindWithTag("Spawner").GetComponent<BalloonSpawning>().DONT_SPAWN = false;
	}
}
