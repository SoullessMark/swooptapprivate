﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {
	public BalloonHitDetection detect;
	// Use this for initialization
	void Start () {
		detect = GameObject.FindWithTag ("Finish").GetComponent<BalloonHitDetection> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (detect.score > detect.hiScore) {
			detect.hiScore = detect.score;
		}
		Text me = GetComponent<Text> ();
		me.text = "hiScore : " + detect.hiScore;
	}
}
