﻿using UnityEngine;
using System.Collections;

public class BOUNCE_WALL : MonoBehaviour {
	private int FORCE_AMOUNT;
	// Use this for initialization
	void Start () {
		if(gameObject.tag == "TopWall"){
			FORCE_AMOUNT = Random.Range (105,126);
		}else{
			FORCE_AMOUNT = Random.Range (-105,-126);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "Bounce"){
			col.transform.eulerAngles = new Vector3(0,0,col.transform.eulerAngles.z + FORCE_AMOUNT);
			Debug.Log("Boop");
			if(gameObject.tag == "TopWall"){
				FORCE_AMOUNT = Random.Range (105,126);
			}else{
				FORCE_AMOUNT = Random.Range (-105,-126);
			}
			col.gameObject.GetComponent<Projectile>().ProjectileSpeed ++;
		}
	}
}
